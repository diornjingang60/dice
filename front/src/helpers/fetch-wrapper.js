import { store } from "../store";


const request = method => {
    return (url, body) => {
        const requestOptions = {
            method,
            headers: authHeader(url)
        };

        if (body) {
            requestOptions.headers['Content-Type'] = 'application/json';
            requestOptions.headers['Accept'] = 'application/json';
            requestOptions.body = JSON.stringify(body);
        }

        return fetch(url, requestOptions).then(handleResponse);
    }
};


const authHeader = url => {
    return { 'x-access-token': store.getState().root.token };
};

const handleResponse = response => {


    return response.text().then(text => {
        const data = text && JSON.parse(text);

        if (!response.ok || data.error) {
            let err = data, errr;

            if ([400, 401, 402, 403, 404, 405, 406].includes(response.status)) {
                if(err.message === 'jwt expired'){
                    errr = {message: "Token has been expired."}
                }else{
                    errr = {message: data.error}
                }
                const error = errr;
                return Promise.reject(error);
            }
        }
        return data;
    });
};

const fetcher = {
    post: request('POST'),
    get: request('GET'),
    update: request('UPDATE'),
    delete: request('DELETE'),
    patch: request('PATCH'),
};

export default fetcher;