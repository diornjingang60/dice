import { useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import logo_dice from '../assets/img/logo_dice.png';
import money from '../assets/img/money.png';
import plus from '../assets/img/plus.png';

import { ModalDeposit, ModalError } from '../components';
import { API_URL } from '../appConfig';
import { authActions } from '../store';

import { initRoom } from '../constants';


const Home = () => {

    const navigate = useNavigate();
    const dispatch = useDispatch();

    const [showModal, setShowModal] = useState(false);


    const user = useSelector(x => x.root.user);
    const token = useSelector(x => x.root.token);

    const rooms = useSelector(x => x.root.rooms);

    const [host, setHost] = useState(false);

    const [modalView, setModalView] = useState(<></>);


    const [room, setRoom] = useState({ ...initRoom });


    useEffect(() => {
        dispatch(authActions.listroom());
        let timer = setTimeout(() => {
        }, 3000);
        return () => {
            clearTimeout(timer);
        };
    }, []);

    const handleChange = e => {
        const { name, value } = e.target;
        setRoom({ ...room, [name]: parseInt(value) });
    };

    const handleCreateRoom = () => {
        const idd = Math.floor(Math.random() * 100000);
        let pl = [];
        let new_room = initRoom;

        if (user.balance >= room.playerMoney) {

            if (room.playerMoney === 0 || room.playerMoney < 500) {
                setModalView(<ModalError
                    showModal={showModal}
                    closeModal={setShowModal}
                    content={`Le solde minimum d'une salle est de 500 Frs.`}
                    type={'error'}
                />);
                setShowModal(true);

                setTimeout(() => {
                    setShowModal(false);
                    setModalView(<></>);
                }, 4000);
            } else {
                if (room.price === 0 || room.price < 50) {
                    setModalView(<ModalError
                        showModal={showModal}
                        closeModal={setShowModal}
                        content={`La mise minimale est de 50 Frs.`}
                        type={'error'}
                    />);
                    setShowModal(true);

                    setTimeout(() => {
                        setShowModal(false);
                        setModalView(<></>);
                    }, 4000);
                } else {
                    if (room.size === 0 || room.size < 2) {
                        setModalView(<ModalError
                            showModal={showModal}
                            closeModal={setShowModal}
                            content={`Le nombre minimum de joueur est 2.`}
                            type={'error'}
                        />);
                        setShowModal(true);

                        setTimeout(() => {
                            setShowModal(false);
                            setModalView(<></>);
                        }, 4000);
                    } else {

                        new_room = room;
                        new_room.id = idd;
                        new_room.name = `Room-${idd}`;
                        new_room.host = user;
                        pl.push(user);
                        new_room.players = [...pl];
                        new_room = roomWinings(new_room);

                        const is_host = rooms.filter(room => room.host.id === user.id);

                        if (is_host.length === 0) {
                            dispatch(authActions.createRoom({
                                id: new_room.id,
                                name: new_room.name,
                                players: new_room.players,
                                size: new_room.size,
                                price: new_room.price,
                                playerMoney: new_room.playerMoney,
                                dices: new_room.dices,
                                host: new_room.host,
                                winings: new_room.winings,
                                cagnotte: new_room.cagnotte,
                                results: new_room.results,
                                token:token
                            }));
                        }
                    }
                }
            }


        } else {
            setModalView(<ModalError
                showModal={showModal}
                closeModal={setShowModal}
                content={`Vous n'avez pas assez d'argent pour creer une salle. Merci de recharger votre compte`}
                type={'error'}
            />);
            setShowModal(true);
            setTimeout(() => {
                setShowModal(false);
                setTimeout(() => {
                    setModalView(<></>);
                }, 2000);
            }, 3000);
            return;
        }


    };


    const handleMakeDeposit = async (amt) => {


        if (amt >= 1000) {

            let { data } = await axios.post(
                `${API_URL}/make-deposit`,
                { balance: amt, email: user.email },
                {
                    headers: {
                        'x-access-token': token
                    }
                }
            );

            if (data.status === "success") {
                setShowModal(false);
                setTimeout(() => {
                    setModalView(<></>);
                    setTimeout(() => {
                        dispatch(authActions.makedeposit({ data }));
                    }, 2000);
                }, 2000);
            }

        } else {
            console.log('Le montant de depot minimum requis est de 1000');
        }
    };

    const handleJoinRoom = roomId => {
        let joinedRoom = null;
        joinedRoom = rooms.filter(room => room.id === roomId)[0];

        let is;

        rooms.filter(room => {
            is = room.players.filter(player => player.id === user.id);
        });

        if (is.length === 0) {
            //joinedRoom.players.push(user);
        }

        console.log(JSON.stringify(rooms));
    };


    return (
        <div className='flex h-[1080px] w-full bg-black justify-center content-center'>
            <div className='flex w-[600px] bg-slate-900 h-[800px] relative flex-col mt-[2%] p-4 text-gray-300 content-center '>
                <div className='flex w-full h-32 justify-center p-2'>
                    <img src={logo_dice} alt="logo_dice" className='w-[50%]' onClick={() => dispatch(authActions.logout())} />
                </div>

                <div className="flex justify-center font-bold text-3xl">
                    <span>Bienvenue {user.name}</span>
                </div>

                <div className="flex justify-between bg-cyan-100 rounded-full w-[50%] self-center mt-4 h-12">
                    <div className="h-8 w-8 rounded-full flex flex-col self-center justify-center items-center ml-1">
                        <img src={money} alt="money_logo" className='h-12 w-12' />
                    </div>

                    <div className='flex flex-col justify-center font-extrabold text-xl text-orange-500'>{user.balance}</div>

                    <button className="h-8 w-8 rounded-full shadow-md shadow-slate-800 self-center mx-2" onClick={() => {
                        setModalView(<ModalDeposit
                            showModal={showModal}
                            handleMakeDeposit={handleMakeDeposit}
                            // closeModal={aa}
                            closeModal={setShowModal}
                        />);
                        setShowModal(true);
                    }}>
                        <img src={plus} alt="plus_logo" className='h-8 w-16' />
                    </button>

                </div>

                <div className='flex w-[90%] flex-col items-center self-center  mt-4 h-[400px] py-4 px-2 gap-4 rounded-[10px]'>

                    {/* create a room */}
                    <div className='flex w-full justify-center h-[60px] p-2 mb-12'>
                        <div className={`flex flex-col`}>
                            <div className='flex mb-2 justify-center'>
                                <div className='flex flex-col w-[35%]'>
                                    <label htmlFor="solde" className='font-extrabold text-sm capitalize'>solde</label>
                                    <input
                                        type="number"
                                        id='solde'
                                        placeholder='Ex:500'
                                        minLength={3}
                                        min={100}
                                        onChange={handleChange}
                                        name='playerMoney'
                                        maxLength={4}
                                        max={10000}
                                        className='ml-1 bg-gray-700 p-[.1rem] rounded-[2px]'
                                    />
                                </div>
                                <div className='flex flex-col w-[35%]'>
                                    <label className='font-extrabold text-sm capitalize' htmlFor="mise">mise</label>
                                    <input onChange={handleChange} className='ml-1 bg-gray-700 p-[.1rem] rounded-[2px]' name='price' type="number" id='mise' placeholder='Ex:100' minLength={2} min={50} maxLength={3} max={1000} />
                                </div>
                                <div className='flex flex-col w-[30%]'>
                                    <label className='font-extrabold text-sm capitalize' htmlFor="joueur">joueur</label>
                                    <input onChange={handleChange} className='ml-1 bg-gray-700 p-[.1rem] rounded-[2px]' type="number" name='size' id='joueur' placeholder='Ex:1' minLength={1} min={1} maxLength={1} max={1} />
                                </div>
                            </div>

                            <div className={`flex
                                flex-col
                                justify-center
                                bg-sky-800
                                self-center
                                p-1
                                shadow-sm
                                shadow-sky-200
                                w-[80%]
                                text-center
                                rounded-full
                                cursor-pointer
                                hover:bg-sky-300
                                hover:shadow-sky-800
                                hover:text-sky-800
                            `}>
                                {
                                    !host
                                        ? <button onClick={handleCreateRoom} className='font-semibold text-[1.1rem]'>
                                            Creer une salle
                                        </button>
                                        : null
                                }

                            </div>
                        </div>
                    </div>

                    {/* Join a Room */}
                    <div className='flex flex-col w-full items-center h-full'>
                        <div className='flex flex-col justify-center items-center w-full mb-2 h-[40px] '>
                            <span>Rejoindre une salle</span>
                        </div>
                        <div className='flex h-[251px] overflow-auto justify-center w-full gap-4 p-4 flex-wrap'>
                            {
                                rooms.map(room => {
                                    return <div key={room.id} className='flex justify-between flex-col bg-gray-800 p-2 h-[75%] w-[130px] rounded-md shadow-sm shadow-slate-500'>
                                        <div className='text-sm'>
                                            <div className='text-center font-extrabold'>
                                                <span className='text-white'>{room.name}</span>
                                            </div>
                                            <div className='flex justify-evenly text-gray-600'>
                                                <div className='font-extrabold'>
                                                    <p>Solde:</p>
                                                    <p>Mise:</p>
                                                    <p>Players:</p>
                                                </div>
                                                <div className='font-extrabold text-green-400'>
                                                    <p>{room.playerMoney}</p>
                                                    <p>{room.price}</p>
                                                    <p>{room.players.length} / {room.size}</p>
                                                </div>
                                            </div>
                                            <div className='mt-1 mb-1 text-center font-semibold flex flex-col'>
                                                <span className='font-light text-gray-600'>Hébergé par:</span>
                                                <span className={`text-xs ${room.host.id === user.id ? 'text-green-400' : 'text-orange-500'}`}>{room.host.id === user.id ? 'Vous' : room.host.name}</span>
                                            </div>
                                        </div>
                                        <button className={`
                                            bg-blue-500
                                            text-center
                                            text-sm
                                            rounded-md
                                            p-1
                                            cursor-pointer
                                            hover:shadow-sm
                                            hover:shadow-blue-500
                                            hover:bg-blue-900
                                            hover:text-blue-400
                                        `} onClick={() => handleJoinRoom(room.id)}>
                                            Aller
                                        </button>
                                    </div>
                                })
                            }


                        </div>
                    </div>
                </div>
            </div>






            {modalView}

        </div>
    );
}

export default Home;


function roomWinings(room) {
    let total = room.players.length * room.price;
    return {
        ...room,
        winings: total
    }
}