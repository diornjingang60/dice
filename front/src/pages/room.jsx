import { useContext, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { SocketContext } from '../context/socket';
import THREE from "../assets/js/three";
import { OrbitControls } from "../assets/js/orbitControls";
import $ from 'jquery';
import CANNON from "cannon";
import { DiceManager, DiceD6 } from "threejs-dice/lib/dice";
import { authActions } from '../store';


const Room = () => {

    const user = useSelector(x => x.root.user);
    const in_room = useSelector(x => x.root.inroom);
    const rooms = useSelector(x => x.root.rooms);

    const [myTurn, setMyTurn] = useState(false);

    const socket = useContext(SocketContext);

    // standard global variables
    var scene,
        camera,
        renderer,
        controls,

        result = 0,
        world,
        ddice = 1,
        dice = [],
        a = 0, b = null;


    const dispatch = useDispatch();





    useEffect(() => {

        socket.on('list-rooms', data => {
            dispatch(authActions.listroom({ data: data }));
            data.forEach(room => {
                room.players.forEach(player => {
                    if (player.id === user.id) {
                        console.log(player.turn);
                        setMyTurn(player.turn);
                    }
                })
            });
        });

        rooms.forEach(room => {
            room.players.forEach(player => {
                if (player.id === user.id) {
                    console.log(player.turn);
                    setMyTurn(player.turn);
                }
            })
        });

        socket.on('print-result', data => {
            // console.log(data);
        });

        socket.on('anim-dice', ({ data, history }) => {
            console.log(history);
            const { roomId, user, val } = data;
            if (a === 0) {
                b = val;
                a++;
            } else {
                a--
                randomDiceThrow(b);
            }

        });


        init();


        return () => {
        };
    }, []);


    const handleRoll = () => {
        // let num = parseInt(Math.random() * 6 + 1);
        let num = 5;
        let usr = rooms.filter(room => room.id === in_room.id)[0].players.filter(player => player.id === user.id)[0]

        socket.emit('roll-dice', { user: usr, val: [num], roomId: in_room.id });
    };






    /* ---------------------------------------------------------------- */
    /*                        PLayinng functions                        */
    /* ---------------------------------------------------------------- */
    function init() {

        // SCENE
        scene = new THREE.Scene();


        // CAMERA
        // var SCREEN_WIDTH = window.innerWidth,
        // SCREEN_HEIGHT = window.innerHeight;
        var SCREEN_WIDTH = 400,
            SCREEN_HEIGHT = 300;
        var VIEW_ANGLE = 45,
            ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT,
            NEAR = 0.01,
            FAR = 20000;


        camera = new THREE.PerspectiveCamera(VIEW_ANGLE, ASPECT, NEAR, FAR);
        scene.add(camera);
        camera.position.set(0, 30, 30);

        // RENDERER
        renderer = new THREE.WebGLRenderer({ antialias: true });
        renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
        // renderer.setSize(400, 300, true);
        renderer.shadowMap.enabled = true;
        renderer.shadowMap.type = THREE.PCFSoftShadowMap;



        $("#ThreeJS").html(renderer.domElement);

        // EVENTS
        // CONTROLS
        controls = new OrbitControls(camera, renderer.domElement);


        let ambient = new THREE.AmbientLight("#ffffff", 0.3);
        scene.add(ambient);

        let directionalLight = new THREE.DirectionalLight("#ffffff", 0.5);
        directionalLight.position.x = -1000;
        directionalLight.position.y = 1000;
        directionalLight.position.z = 1000;
        scene.add(directionalLight);

        let light = new THREE.SpotLight(0x0a0a0a, 1.3);
        light.position.y = 100;
        light.position.x = 50;
        light.target.position.set(0, 0, 0);
        light.castShadow = true;
        light.shadow.camera.near = 50;
        light.shadow.camera.far = 110;
        light.shadow.mapSize.width = 1024;
        light.shadow.mapSize.height = 1024;
        scene.add(light);

        // FLOOR
        var floorMaterial = new THREE.MeshPhongMaterial({
            color: "#42270b",
            side: THREE.DoubleSide
        });
        var floorGeometry = new THREE.PlaneGeometry(50, 50, 5, 5);
        var floor = new THREE.Mesh(floorGeometry, floorMaterial);
        floor.receiveShadow = true;
        floor.rotation.x = Math.PI / 2;
        scene.add(floor);
        // SKYBOX/FOG
        var skyBoxGeometry = new THREE.CubeGeometry(10000, 10000, 10000);
        var skyBoxMaterial = new THREE.MeshPhongMaterial({
            color: 0x000000,
            side: THREE.BackSide
        });
        var skyBox = new THREE.Mesh(skyBoxGeometry, skyBoxMaterial);
        scene.add(skyBox);
        scene.fog = new THREE.FogExp2(0x000000, 0.00025);

        ////////////
        // CUSTOM //
        ////////////
        world = new CANNON.World();

        world.gravity.set(0, -120, 0);
        world.broadphase = new CANNON.NaiveBroadphase();
        world.solver.iterations = 16;

        DiceManager.setWorld(world);

        //Floor
        let floorBody = new CANNON.Body({
            mass: 0,
            shape: new CANNON.Plane(),
            material: DiceManager.floorBodyMaterial
        });
        floorBody.quaternion.setFromAxisAngle(
            new CANNON.Vec3(1, 0, 0),
            -Math.PI / 2
        );
        world.add(floorBody);

        //Walls
        dice = [];
        for (var i = 0; i < ddice; i++) {
            var die = new DiceD6({ size: 2.8, backColor: "#fff" });
            scene.add(die.getObject());
            dice.push(die);
        }

        requestAnimationFrame(animate);
    }

    function randomDiceThrow(num) {
        var diceValues = [];
        result = 0;

        for (var i = 0; i < dice.length; i++) {
            let yRand = Math.random() * 20;
            dice[i].getObject().position.x = -15 - (i % 3) * 1.5;
            dice[i].getObject().position.y = 2 + Math.floor(i / 3) * 1.5;
            dice[i].getObject().position.z = -15 + (i % 3) * 1.5;
            dice[i].getObject().quaternion.x =
                ((Math.random() * 90 - 45) * Math.PI) / 180;
            dice[i].getObject().quaternion.z =
                ((Math.random() * 90 - 45) * Math.PI) / 180;
            dice[i].updateBodyFromMesh();
            let rand = Math.random() * 5;
            dice[i]
                .getObject()
                .body.velocity.set(25 + rand, 40 + yRand, 15 + rand);
            dice[i]
                .getObject()
                .body.angularVelocity.set(
                    20 * Math.random() - 10,
                    20 * Math.random() - 10,
                    20 * Math.random() - 10
                );

            diceValues.push({ dice: dice[i], value: num[i] });
            result = result + num[i];
        }

        DiceManager.prepareValues(diceValues);

        // console.log(result);
        // setTimeout(() => {
        // }, 2000);
    }

    function animate() {
        updatePhysics();
        render();
        update();

        requestAnimationFrame(animate);
    }

    function updatePhysics() {
        world.step(1.0 / 60.0);

        for (var i in dice) {
            dice[i].updateMeshFromBody();
        }
    }

    function update() {
        controls.update();

    }

    function render() {
        renderer.render(scene, camera);
    }


    return in_room
        ? (
            <div className='flex h-[1080px] w-full bg-black justify-center content-center'>
                <div className='flex w-[600px] bg-slate-900 h-[800px] relative flex-col mt-[2%] p-4 text-gray-300 content-center '>

                    <div className='flex w-full h-32 justify-center p-2'>
                        <div className="flex justify-center flex-col font-bold text-3xl w-[70%]">
                            <div className='text-center'>{in_room.name}</div>
                            <div className='bg-slate-700 w-full self-center p-2 rounded-md'>
                                <div>
                                    <div>Players: {`${in_room.players.length}/${in_room.size}`}</div>
                                    <div>
                                        <ul>
                                            {in_room.players.map(player => <li key={player.name}>{player.name} <span className='text-xs text-lime-400'>{player.name === in_room.host.name ? 'Master' : ''}</span></li>)}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className='bg-slate-900 w-[90%] h-[60%] mt-5 flex content-center'>
                        <div id="ThreeJS" className=''></div>
                    </div>



                    {
                        myTurn
                            ? <button onClick={handleRoll}
                                className={`
                                    bg-green-500
                                    text-center
                                    text-sm
                                    rounded-md
                                    py-3
                                    mt-3
                                    px-4
                                    w-[40%]
                                    self-center
                                    cursor-pointer
                                    hover:shadow-sm
                                    hover:shadow-green-500
                                    hover:bg-green-800
                                    hover:text-green-400
                                `}>lancer</button>
                            : <button disabled
                                className={`
                                bg-red-400
                                text-center
                                text-sm
                                text-red-700
                                rounded-md
                                py-3
                                mt-3
                                px-4
                                w-[40%]
                                self-center
                            `}>Wait...</button>
                    }

                </div>
            </div>
        )
        : window.location.href = '/';
}

export default Room;
