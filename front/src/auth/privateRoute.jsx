import { Navigate, Outlet } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { history } from '../helpers';
import { authActions } from '../store';


const PrivateRoute = () => {
    const authUser = useSelector(x => x.root.token ? true : false);

    const dispatch = useDispatch();


    if (!authUser) {
        // not logged in so redirect to login page with the return url
        return <Navigate to="/login" state={{ from: history.location }} />
    } else {
        dispatch(authActions.getdata());

        // authorized so return child components
        return <Outlet />;
    }
}

export default PrivateRoute;





const RoomRoute = () => {

    return <Outlet />;
}

export { RoomRoute };
