export { default as SignIn } from './login';
export { default as SignUp } from './register';
export { default as PrivateRoute } from './privateRoute';
