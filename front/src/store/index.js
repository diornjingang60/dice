import { combineReducers, configureStore } from '@reduxjs/toolkit';

import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import {thunk} from 'redux-thunk';

import { authReducer } from './auth.slice';


export * from './auth.slice';


const reducers = combineReducers({
    root: authReducer,
});

const persistConfig = {
    key: 'root',
    storage
};

const persistedReducer = persistReducer(persistConfig, reducers);


export const store = configureStore({
    reducer: persistedReducer,
    devTools: true,
    middleware: () => [thunk],
});
