import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { API_URL } from '../appConfig';
import $ from 'jquery';
import { history } from '../helpers';
import fetcher from '../helpers/fetch-wrapper';
import axios from 'axios';

// create slice

const name = 'root';
const initialState = createInitialState();
const reducers = createReducers();
const extraActions = createExtraActions();
const extraReducers = createExtraReducers();
const slice = createSlice({ name, initialState, reducers, extraReducers });

// exports

export const authActions = { ...slice.actions, ...extraActions };
export const authReducer = slice.reducer;

// implementation

function createInitialState() {
    return {
        // initialize state from local storage to enable user to stay logged in
        token: null,
        user: {},
        inroom: null,
        rooms: [],
        error: null,
    }
}

function createReducers() {
    return {
        logout
    };

    function logout(state) {
        state.token = null;
        state.user = {};
        state.inroom = null;
        state.rooms = [];
        localStorage.removeItem('persist:root');
        history.navigate('/login');
    }
}

function createExtraActions() {
    const baseUrl = API_URL;

    return {
        login: login(),
        register: register(),
        getdata: getdata(),
        makedeposit: makeDeposit(),
        listroom: listRooms(),
        createRoom: createRoom(),
        setInRoom: setInRoom(),
    };

    function login() {
        return createAsyncThunk(
            `${name}/login`,
            // async ({ email, password }) => {
            //     return await $.post(
            //         `${baseUrl}/login`,
            //         { email, password },
            //         data => { return data }
            //     )
            // }

            async ({ email, password }) => {
                return await fetcher.post(
                    `${baseUrl}/login`,
                    { email, password })
            }
        );
    }

    function register() {
        return createAsyncThunk(
            `${name}/register`,
            async ({ name, email, phone, password }) => {
                return await $.post(
                    `${baseUrl}/register`,
                    { name, email, phone, password },
                    data => { return data }
                )
            }
        );
    }

    function getdata() {
        return createAsyncThunk(
            `${name}/getuser`,
            async () => {
                return await fetcher.get(`${baseUrl}/getuser`);
            })
    }

    function makeDeposit() {
        return createAsyncThunk(
            `${name}/make/deposit`,
            ({ data }) => {
                return data;
            })
    }

    function setInRoom() {
        return createAsyncThunk(
            `${name}/set/in/room`,
            room => {
                return room;
            })
    }

    function listRooms() {
        return createAsyncThunk(
            `${name}/list/rooms`,
            ({ data }) => {
                return data;
            })
    }

    function createRoom() {
        return createAsyncThunk(
            `${name}/create/room`,
            async ({ id, name,
                players, size,
                price, playerMoney,
                dices, host,
                winings, cagnotte,
                results, token
            }) => {
                let { data } = await axios.post(
                    `${baseUrl}/create-room`,
                    {
                        id, name,
                        players, size,
                        price, playerMoney,
                        dices, host,
                        winings, cagnotte,
                        results
                    },
                    {
                        headers: {
                            'x-access-token': token
                        }
                    }
                );
                return data;
            }

        )
    }
}














function createExtraReducers() {
    return (builder) => {
        login();
        register();
        getdata();
        makedeposit();
        createRoom();
        listRooms();
        setInRoom();


        function login() {
            var { pending, fulfilled, rejected } = extraActions.login;
            builder
                .addCase(pending, (state) => {
                    state.error = null;
                })
                .addCase(fulfilled, (state, action) => {
                    const data = action.payload;
                    state.token = data.token;
                    setTimeout(() => {
                        window.location.href = '/';
                    }, 3000);
                })
                .addCase(rejected, (state, action) => {
                    console.log("login", action);
                })
        };

        function register() {
            var { pending, fulfilled, rejected } = extraActions.register;
            builder
                .addCase(pending, (state) => {
                    state.error = null;
                })
                .addCase(fulfilled, (state, action) => {
                    setTimeout(() => {
                        history.navigate('/login');
                    }, 1800);
                })
                .addCase(rejected, (state, action) => {
                    state.error = action.error;
                    console.log("register", action.payload.error);

                })
        };




        function getdata() {
            var { pending, fulfilled, rejected } = extraActions.getdata;
            builder
                .addCase(pending, (state) => {
                    // state.error = null;
                })
                .addCase(fulfilled, (state, action) => {
                    state.user = action.payload.user;
                })
                .addCase(rejected, (state, action) => {
                    if (action.meta.requestStatus === 'rejected') {
                        localStorage.removeItem('persist:root');
                        window.location.reload();
                    }
                })
        };

        function makedeposit() {
            var { pending, fulfilled, rejected } = extraActions.makedeposit;
            builder
                .addCase(pending, (state) => {
                    // state.error = null;
                })
                .addCase(fulfilled, (state, action) => {
                    state.user = action.payload.user;
                    // console.log(action.payload.user)
                })
                .addCase(rejected, (state, action) => {
                    if (action.meta.requestStatus === 'rejected') {
                        // localStorage.removeItem('persist:root');
                        // window.location.reload();
                    }
                })
        };


        function setInRoom() {
            var { pending, fulfilled, rejected } = extraActions.setInRoom;
            builder
                .addCase(pending, (state) => {
                    // state.error = null;
                })
                .addCase(fulfilled, (state, action) => {
                    state.inroom = action.payload;
                    // console.log(action.payload)
                })
                .addCase(rejected, (state, action) => {
                    if (action.meta.requestStatus === 'rejected') {
                        state.inroom = null;
                        // localStorage.removeItem('persist:root');
                        // window.location.reload();
                    }
                })
        };



        function listRooms() {
            var { pending, fulfilled, rejected } = extraActions.listroom;
            builder
                .addCase(pending, (state) => {
                    // state.error = null;
                })
                .addCase(fulfilled, (state, action) => {
                    // console.log(action.payload);
                    state.rooms = action.payload;
                })
                .addCase(rejected, (state, action) => {
                    if (action.meta.requestStatus === 'rejected') {
                        // localStorage.removeItem('persist:root');
                        state.rooms = [];
                        //window.location.reload();
                    }
                })
        };

        function createRoom() {
            var { pending, fulfilled, rejected } = extraActions.createRoom;
            builder
                .addCase(pending, (state) => {
                    // state.error = null;
                })
                .addCase(fulfilled, (state, action) => {
                    // console.log(action.payload);
                    state.rooms = [...state.rooms, action.payload];
                    // console.log(action.payload.user)
                })
                .addCase(rejected, (state, action) => {
                    if (action.meta.requestStatus === 'rejected') {
                        // localStorage.removeItem('persist:root');
                        // window.location.reload();
                    }
                })
        };

    }
}