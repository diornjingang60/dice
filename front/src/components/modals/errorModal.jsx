import { Button, Modal } from 'flowbite-react';
import { HiOutlineExclamationCircle } from 'react-icons/hi';
import { VscError } from "react-icons/vsc";

const ModalError = ({ showModal, closeModal, size = 'md', position = 'center', content, type }) => {


    if (type) {
        type = "text-red-600 dark:text-red-300";
    } else {
        type = "text-lime-600 dark:text-lime-300";
    }

    return <Modal show={showModal} size={size} position={position} onClose={() => closeModal(false)} popup>
        <Modal.Header />
        <Modal.Body >
            <div className="text-center">
                <VscError className={`mx-auto mb-4 h-14 w-14 ${type}`} />
                <h3 className="mb-5 text-lg font-normal text-gray-500 dark:text-gray-400">
                    {content}
                </h3>
                <div className="flex justify-center gap-4">
                    {/* <Button color="failure" onClick={() => closeModal(false)}>
                        {"Yes, I'm sure"}
                    </Button> */}
                    <Button color="gray" onClick={() => closeModal(false)}>
                        Close
                    </Button>
                </div>
            </div>
        </Modal.Body>
    </Modal>
};

export default ModalError;