export const initRoom = {
    id: "",
    name: "", //room name
    players: [], //array of players in the room
    size: 0, // size of array player
    price: 0,
    playerMoney: 0, //player needed money to join the room
    dices: 1,
    host: null, // celui qui cree la salle
    winings: "0",
    cagnotte: "0", //total d'argent de tous les joueurs une fois dans la room
    results: []
};
