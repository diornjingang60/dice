
import { useEffect } from 'react';

import {
	Routes as Switch,
	Route,
	useNavigate,
	useLocation
} from "react-router-dom";

import { history } from './helpers';
import {
	PrivateRoute,
	SignIn,
	SignUp
} from "./auth";
import { Home } from './pages';
import Room from './pages/room';
import { RoomRoute } from './auth/privateRoute';

const App = () => {

	history.location = useLocation();
	history.navigate = useNavigate();

	useEffect(() => {
		// socket.emit('joingame', {});
		return () => {
			// socket.off('joingame');
		};
	}, []);



	return <Switch>
		<Route path="/login" element={<SignIn />} />
		<Route path="*" element={<SignIn />} />
		<Route path="/register" element={<SignUp />} />

		<Route element={<PrivateRoute />}>
			<Route path='/' element={<Home />} />
			<Route path='*' element={<Home />} />

			<Route element={<RoomRoute  />}>
				<Route path='/room' element={<Room />} />
			</Route>
		</Route>
	</Switch>;

};

export default App;
