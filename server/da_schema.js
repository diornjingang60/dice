const create_users_table = `CREATE TABLE IF NOT EXISTS users (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT, 
    email TEXT UNIQUE,
    phone TEXT UNIQUE,
    balance INTEGER DEFAULT 0,
    password TEXT, 
    token TEXT NULL,
    connected INTEGER DEFAULT 0,
    updated_at TEXT, 
    created_at TEXT
)`;


const create_rooms_table = `CREATE TABLE IF NOT EXISTS rooms (
    id INTEGER PRIMARY KEY,
    name TEXT, 
    players TEXT,
    size INTEGER,
    price INTEGER,
    playerMoney INTEGER,
    dices INTEGER,
    host TEXT,
    winings FLOAT,
    cagnotte INTEGER,
    results TEXT,
    closed INTEGER DEFAULT 0,
    updated_at TEXT, 
    created_at TEXT
)`;


const stm_add_user = 'INSERT INTO users (name, email, phone, password, updated_at, created_at) VALUES (?,?,?,?,?,?)';
const stm_add_room = 'INSERT INTO rooms (id, name, players, size, price, playerMoney, dices, host, winings, cagnotte, results, updated_at, created_at) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)';


const stm_get_user = 'SELECT id, name, email, phone, balance, password, connected, updated_at, created_at FROM users WHERE email = ?';
const stm_get_rooms = 'SELECT * FROM rooms';


const stm_update_set_token = 'UPDATE users SET token = COALESCE(?,token), connected = 1, updated_at = COALESCE(?,updated_at) WHERE email = COALESCE(?,email)';
const stm_update_set_token_logout = 'UPDATE users SET token = NULL, connected = 0, updated_at = COALESCE(?,updated_at) WHERE email = COALESCE(?,email)';
const stm_update_set_balance = 'UPDATE users SET balance = COALESCE(?,balance), updated_at = COALESCE(?,updated_at) WHERE email = COALESCE(?,email)';
const stm_update_room = 'UPDATE rooms SET players = COALESCE(?,players), results=COALESCE(?,results), closed=COALESCE(?,closed), updated_at=COALESCE(?,updated_at)';









module.exports = {
    //Create tables
    create_users_table,
    create_rooms_table,


    //Add data to tables
    stm_add_user,
    stm_add_room,


    //Retrieve data from tables
    stm_get_user,
    stm_get_rooms,



    //Update data in tablee
    stm_update_set_token,
    stm_update_set_token_logout,
    stm_update_set_balance,
    stm_update_room,

};
