require('dotenv').config();
const routes = require('./routes.js');

const path = require('path');
var express = require('express');
var bodyParser = require('body-parser');

const http = require('http');
const cors = require('cors');
const { Server } = require('socket.io');

const PORT = 4000;





const app = express();

app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(cors());  // Add cors middleware


const server = http.createServer(app);


const io = new Server(server, {
    cors: {
        origin: '*',
        methods: ['GET', 'POST', 'DELETE', 'UPDATE', 'PATCH']
    },
});

const game_url = 'http://localhost:5173';

let initRoom = {
    id: "",
    name: "", //room name
    players: [], //array of players in the room
    size: 0, // size of array player
    price: 0,
    playerMoney: 0, //player needed money to join the room
    dices: 2,
    host: null, // celui qui cree la salle
    winings: 0,
    cagnotte: 0, //total d'argent de tous les joueurs une fois dans la room
    results: []
};

// Add this
let Rooms = [], gameHistory = []; // {id:roomId, data:[]}



io.on('connection', (socket) => {
    console.log(`User connected ${socket.id}`);

    socket.emit('list-rooms', Rooms);


    socket.on('disconnect', () => {
        console.log('User disconnected:', socket.id);
    });




    // Creer une salle
    socket.on('create-room', data => {
        if (data.user) {
            let pl = [];
            let new_room = initRoom;

            socket.join(`${socket.id}`);

            new_room.id = socket.id;
            new_room.name = `Room-${socket.id.slice(0, 8)}`;
            new_room.playerMoney = data.playerMoney;
            new_room.price = parseInt(data.price);
            new_room.size = parseInt(data.size);
            new_room.host = data.user;
            pl.push({ ...data.user, turn: true, has_roll: false }); // turn a true pour montrer le host de la salle est le premier a jouer
            new_room.players = [...pl];
            new_room = roomWinings(new_room);

            const is_host = Rooms.filter(room => room.host.id === data.user.id);

            if (is_host.length === 0) {
                Rooms.push(new_room);
            }


            // io.to(socket.id).emit('init-room', new_room);

            new_room = [];

            pl = [];

            io.emit('list-rooms', Rooms);

            gameHistory.push({ id: socket.id, data: [] });
        }
        // console.log(Rooms);
    });



    //rejoindre une salle
    socket.on('join-room', data => {
        const { roomId, user } = data;

        let _room = Rooms.filter(room => room.id === roomId)[0];

        socket.join(`${roomId}`);




        let already_in = Rooms.filter(room => room.id === roomId)[0].players.filter(player => player.id === user.id).length !== 0;

        if (!already_in) {
            if (_room.players.length < _room.size) {
                // if (_room.host.id === user.id) {
                //     _room.players = [..._room.players, { ...user, has_roll: false, turn: true }];
                // }else{
                // }
                _room.players = [..._room.players, { ...user, has_roll: false, turn: false }];
            }
        }

        if (parseInt(_room.size) === parseInt(_room.players.length)) {
            console.log('Ok room is full');



            io.to(_room.id).emit('init-room');
        }


        // io.to(`${roomId}`).emit('init-room', _room);
        // if (parseInt(_room.size) === parseInt(_room.players.length)) {
        //     console.log('emited to init-room');
        // }


        io.emit('list-rooms', Rooms);
    });



    socket.on('roll-dice', data => {
        let { roomId, user, val } = data;

        let room = Rooms.filter(room => room.id === roomId)[0];

        // console.log("rool dice", data);
        let pl = Rooms.filter(room => room.id === roomId)[0].players;
        let player = Rooms.filter(room => room.id === roomId)[0].players.filter(player => player.id === user.id)[0];
        player.turn = false;
        player.has_roll = true;

        let player_not_rolled = Rooms.filter(room => room.id === roomId)[0].players.filter(player => player.has_roll === false);

        if (player_not_rolled.length !== 0) {
            player_not_rolled[0].turn = true;
        } else {
            // console.log(player_not_rolled);
        }


        let histo = gameHistory.filter(hist => hist.id === roomId)[0].data;

        histo.push({ player, val: val[0] });
        // console.log(Rooms.filter(room => room.id === roomId)[0].players);

        if (room.size === histo.length) {
            let same_value = 0, temp_var;
            histo.forEach(player => {
                console.log(player);
                same_value = same_value - player.val;
                // console.log(pl.filter(_pl => _pl.id === player.id)[0]);
            });

        }

        io.to(roomId).emit('anim-dice', { data, history: gameHistory });
        io.to(roomId).emit('list-rooms', Rooms);

    });

    socket.on('show-result', data => {
        io.emit('print-result', data);
    })


    // Delete a room
    socket.on('delete-room', id => {


    });






    // app.get('/', (req, res, next) => {
    //     res.send(`<h1>Welcome to the game.</h1><br /><span><a href=${game_url}>Click here to go to game</a></span>`);
    // });


    app.get('/', (req, res, next) => {
        // res.send(path.join(__dirname, 'public', 'index.html'));
    });

    routes(app).login();

    routes(app).register();

    routes(app).getUser();

    routes(app).logout();

    routes(app).makeDeposit();

    routes(app).createRoom();

    routes(app).listRooms();



});


server.listen(PORT, () => console.log(`Server is running on port:${PORT}`));


function roomWinings(room) {
    let total = room.players.length * room.price;
    return {
        ...room,
        winings: total
    }
}
