require('dotenv').config();
const routes = require('./routes.js');

const path = require('path');
var express = require('express');
var bodyParser = require('body-parser');

const http = require('http');
const cors = require('cors');
const { Server } = require('socket.io');

const PORT = 4000;





const app = express();

// app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(cors());  // Add cors middleware


const server = http.createServer(app);


const io = new Server(server, {
    cors: {
        origin: '*',
        methods: ['GET', 'POST', 'DELETE', 'UPDATE', 'PATCH']
    },
});

const game_url = 'http://localhost:5173';

let initRoom = {
    id: "",
    name: "", //room name
    players: [], //array of players in the room
    size: 0, // size of array player
    price: 0,
    playerMoney: 0, //player needed money to join the room
    dices: 1,
    host: null, // celui qui cree la salle
    winings: 0,
    cagnotte: 0, //total d'argent de tous les joueurs une fois dans la room
    results: []
};

// Add this
let Rooms = [];
let usersInRoom = [];


io.on('connection', (socket) => {
    console.log(`User connected ${socket.id}`);




    socket.on('disconnect', () => {
        console.log('User disconnected:', socket.id);
    });

    socket.broadcast.emit('listrooms', {
        rooms: Rooms
    });

    socket.on('listrooms', (data = null) => {
        socket.broadcast.emit('listrooms', {
            rooms: Rooms
        });
    })

    // Creer une salle
    socket.on('createroom', data => {
        if (data.id !== undefined) {
            let pl = [];
            let new_room = initRoom;

            new_room.id = data.id;
            new_room.name = `Room-${data.id}`;
            new_room.dices = data.dice;
            new_room.playerMoney = data.playerMoney;
            new_room.price = data.price;
            new_room.size = data.size;
            new_room.host = data.user;
            pl.push(data.user);
            new_room.players = [...pl];
            new_room = roomWinings(new_room);

            const is_host = Rooms.filter(room => room.host.id === data.user.id);

            if (is_host.length === 0) {
                Rooms.push(new_room);
            }

            new_room = [];

            pl = [];

            socket.broadcast.emit('listrooms', {
                rooms: Rooms
            });

            console.log(Rooms);

        } else {
            console.log('undefined id');
        }
    });

    //rejoindre une salle
    // socket.on('joinroom', data => {
    //     let joinedRoom = null;
    //     joinedRoom = Rooms.filter(room => room.id = data.roomId)[0];

    //     let is;

    //     Rooms.filter(room => {
    //         is = room.players.filter(player => player.id === data.userId);
    //     });

    //     if (is.length === 0) {
    //         joinedRoom.players.push(user);
    //     }


    // });







    app.get('/', (req, res, next) => {
        res.send(`<h1>Welcome to the game.</h1><br /><span><a href=${game_url}>Click here to go to game</a></span>`);
    });

    routes(app).login();

    routes(app).register();

    routes(app).getUser();

    routes(app).makeDeposit();

    routes(app).createRoom();

    routes(app).listRooms();



});


server.listen(PORT, () => console.log(`Server is running on port:${PORT}`));


function roomWinings(room) {
    let total = room.players.length * room.price;
    return {
        ...room,
        winings: total
    }
}
