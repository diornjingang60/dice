require("dotenv").config();

var md5 = require("md5");
const auth = require("./middleware/auth");
var db = require("./database.js");
const jwt = require("jsonwebtoken");

const {
    stm_add_user,
    stm_get_user,
    stm_update_set_token,
    stm_update_set_balance,
    stm_add_room,
    stm_get_rooms,
    stm_update_set_token_logout
} = require("./da_schema.js");


module.exports = (app = null) => {


    return {

        /* ---------------------------------------------------------------- */
        /*                             register                             */
        /* ---------------------------------------------------------------- */
        register: () => app.post('/api/register', (req, res, next) => {


            const d = new Date();
            const now = `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()} ${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`;
            const update_at = now;
            const created_at = now;

            var errors = [];

            if (!req.body.password) {
                errors.push("No password specified");
            }

            if (!req.body.email) {
                errors.push("No email specified");
            }

            if (!req.body.phone) {
                errors.push("No phone number specified");
            }

            if (errors.length) {
                res.status(400).json({ "error": errors.join(",") });
                return;
            }

            // var e = null;

            db.get(stm_get_user, [req.body.email], (err, row) => {
                if (err) {
                    res.status(400).json({ "error": err.message });
                    return;
                }

                if (row === undefined) {

                    var data = {
                        name: req.body.name,
                        email: req.body.email,
                        phone: req.body.phone,
                        update_at: update_at,
                        created_at: created_at
                    };

                    var params = [data.name, data.email, data.phone, md5(req.body.password), data.update_at, data.created_at];

                    db.run(stm_add_user, params, (err, result) => {
                        if (err) {
                            res.status(400).json({ "error": err.message });
                            return;
                        }

                        db.get(stm_get_user, [req.body.email], (err, row) => {
                            const d = new Date();
                            const now = `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()} ${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`;

                            if (err) {
                                res.status(400).json({ "error": err.message });
                                return;
                            }

                            res.json({
                                "message": "User created successfully!"
                            });

                        });
                    });
                } else {
                    res.status(400).json({ "error": "The user already exist" });
                    console.log('The user already exist')
                }
            });
        }),

        /* ---------------------------------------------------------------- */
        /*                               login                              */
        /* ---------------------------------------------------------------- */
        login: () => app.post('/api/login', (req, res, next) => {
            const { email, password } = req.body;

            const d = new Date();
            const now = `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()} ${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`;
            const update_at = now;


            if (!(email && password)) {
                res.status(400).send("All input are required");
            }


            db.get(stm_get_user, [email], (err, row) => {
                if (err) {
                    res.status(400).json({ "error": err.message });
                    return;
                }

                if (row && row.password === md5(password)) {
                    if(row.connected === 1){
                        const token = jwt.sign(
                            {
                                id: row.id,
                                email: row.email,
                                name: row.name,
                                pone: row.pone,
                                created_at: row.created_at,
                                updated_at: row.updated_at
                            },
                            process.env.JWT_KEY,
                            {
                                expiresIn: "2h",
                            }
                        );
    
                        db.run(
                            stm_update_set_token,
                            [token, update_at, email],
                            (err, result) => {
                                if (err) {
                                    res.status(400).json({ "error": res.message })
                                    return;
                                }
                                res.json({
                                    status: 'success',
                                    token: token
                                });
                            }
                        );
                    }else{
                        res.status(403).json({ "message": "User is already connected" });
                        console.log('User is already connected');
                    }

                } else {
                    res.status(404).json({ "error": "User not found" });
                    console.log('User not found');
                }
            });
        }),

        /* ---------------------------------------------------------------- */
        /*                             get user                             */
        /* ---------------------------------------------------------------- */
        getUser: () => app.get('/api/getuser', auth, (req, res, next) => {

            db.get(stm_get_user, [req.user.email], (err, row) => {
                if (err) {
                    res.status(400).json({ "error": err.message });
                    return;
                }

                const { id, name, email, phone, balance, connected, updated_at, created_at } = row;

                res.json({ status: 'success', user: { id, name, email, phone, balance, connected, updated_at, created_at } });
            });
        }),


        /* ---------------------------------------------------------------- */
        /*                              logout                              */
        /* ---------------------------------------------------------------- */
        logout: () => app.post('/api/logout', auth, (req, res, next) => {
            const d = new Date();
            const now = `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()} ${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`;

            db.run(
                stm_update_set_token_logout,
                [now, req.body.email],
                (err, result) => {
                    if (err) {
                        res.status(400).json({ "error": res.message })
                        return;
                    }
                    res.json({
                        status: 'success',
                        message: "Successfully logout"
                    });
                });

        }),


        /* ---------------------------------------------------------------- */
        /*                          Make a deposit                          */
        /* ---------------------------------------------------------------- */
        makeDeposit: () => app.post('/api/make-deposit', auth, (req, res, next) => {
            const d = new Date();
            const now = `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()} ${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`;
            const update_at = now;

            db.run(
                stm_update_set_balance,
                [req.body.balance, update_at, req.body.email],
                (err, result) => {
                    if (err) {
                        res.status(400).json({ "error": res.message })
                        return;
                    }

                    db.get(stm_get_user, [req.user.email], (err, row) => {
                        if (err) {
                            res.status(400).json({ "error": err.message });
                            return;
                        }

                        const { id, name, email, phone, balance, updated_at, created_at } = row;

                        res.json({ status: 'success', user: { id, name, email, phone, balance, updated_at, created_at } });
                    });
                });

        }),


        /* ---------------------------------------------------------------- */
        /*                     create a room in database                    */
        /* ---------------------------------------------------------------- */
        createRoom: () => app.post('/api/create-room', auth, (req, res, next) => {
            const d = new Date();
            const now = `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()} ${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`;

            // req.body.id
            // req.body.name
            // req.body.players
            // req.body.size
            // req.body.price
            // req.body.playerMoney
            // req.body.dices
            // req.body.host
            // req.body.winings
            // req.body.cagnotte
            // req.body.results
            // closed
            // updated_at
            // created_at


            var errors = [];

            if (!req.body.id) {
                errors.push("Room id is not specified");
            }
            if (!req.body.name) {
                errors.push("Room name is not specified");
            }


            if (!req.body.players) {
                errors.push("Room players is not specified");
            }

            if (!req.body.size) {
                errors.push("Room size is not specified");
            }

            if (!req.body.price) {
                errors.push("Room price is not specified");
            }

            if (!req.body.playerMoney) {
                errors.push("Room playerMoney is not specified");
            }

            if (!req.body.dices) {
                errors.push("Room dices is not specified");
            }

            if (!req.body.host) {
                errors.push("Room host is not specified");
            }

            if (!req.body.winings) {
                errors.push("Room winings is not specified");
            }

            if (!req.body.cagnotte) {
                errors.push("Room cagnotte is not specified");
            }

            if (!req.body.results) {
                errors.push("Room results is not specified");
            }

            if (errors.length) {
                res.status(400).json({ "error": errors.join(",") });
                return;
            }

            var params = [
                req.body.id,
                req.body.name,
                JSON.stringify(req.body.players),
                req.body.size,
                req.body.price,
                req.body.playerMoney,
                req.body.dices,
                JSON.stringify(req.body.host),
                req.body.winings,
                req.body.cagnotte,
                JSON.stringify(req.body.results),
                now,
                now
            ];


            db.run(stm_add_room, params, (err, result) => {
                if (err) {
                    res.status(400).json({ "error": err.message });
                    return;
                }
                db.get(stm_get_rooms, [], (err, row) => {
                    if (err) {
                        res.status(400).json({ "error": err.message });
                        return;
                    }
                    res.json({
                        id: row.id,
                        name: row.name,
                        players: JSON.parse(row.players),
                        size: row.size,
                        price: row.price,
                        playerMoney: row.playerMoney,
                        dices: row.dices,
                        host: JSON.parse(row.host),
                        winings: row.winings,
                        cagnotte: row.cagnotte,
                        results: JSON.parse(row.results),
                        closed: row.closed,
                    });
                });
            });

        }),

        /* ---------------------------------------------------------------- */
        /*                             get user                             */
        /* ---------------------------------------------------------------- */
        listRooms: () => app.get('/api/list-rooms', (req, res, next) => {

            db.all(stm_get_rooms, [], (err, rows) => {
                if (err) {
                    res.status(400).json({ "error": err.message });
                    return;
                }
                console.log(rows)
                // res.json({ status: 'success', user: { id, name, email, phone, balance, updated_at, created_at } });
            });


        }),

    }

}


