var sqlite3 = require('sqlite3').verbose();

var md5 = require('md5');


const {
    create_users_table, create_rooms_table,
} = require('./da_schema');

const DBSOURCE = "db.sqlite";

let db = new sqlite3.Database(DBSOURCE, err => {
    if (err) {
        console.error(err.message);
        throw err;
    } else {
        console.log('Connected to the SQlite database.');

        db.run(create_users_table, (err) => {
            if (err) {
                // Table already created
                console.error(err)
            } else {
                // db.run(stm_add_user, ["Ronal Dior", "Password", "admin@gmail.com", "2023-10-11", "2023-10-11"]);
            }
        });

        db.run(create_rooms_table, (err) => {
            if (err) {
                // Table already created
                console.error(err)
            } else {
                // db.run(stm_add_user, ["Ronal Dior", "Password", "admin@gmail.com", "2023-10-11", "2023-10-11"]);
            }
        });
    }
});

module.exports = db;